<?php

use Illuminate\Database\Seeder;

class ArtistesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('artistes')->insert([
        [
            'nom' => 'Allen',
            'prenom' => 'Woody',
            'annee_naissance' => 1938,
        ],[
            'nom' => 'Lynch',
            'prenom' => 'David',
            'annee_naissance' => 1946,
        ]]);
    }
}
