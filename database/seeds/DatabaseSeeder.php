<?php

use Illuminate\Database\Seeder;
use App\Models\Film;
use App\Models\Artiste;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call(ArtistesTableSeeder::class);
        $this->call(FilmsTableSeeder::class);
        /*
        $film = new Film([
            'titre' => 'Mad Max: Fury Road',
            'annee' => 2015,
            'artiste_id' => Artiste::where('nom','Miller')->first()->id
        ]);
    
        $film->save();
    
        $acteur = Artiste::where('nom','Hardy')->where('prenom','Tom')->first();
        $film->acteurs()->attach($acteur->id);
    
        $acteur = Artiste::where('nom','Theron')->where('prenom','Cherlize')->first();
        $film->acteurs()->attach($acteur->id); --> */
    }
}

