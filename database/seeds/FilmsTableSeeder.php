<?php

use Illuminate\Database\Seeder;
use App\Models\Film;

class FilmsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('films')->insert([
        [
            'id' => 1,
            'artiste_id' => 1,
            'titre' => 'Grand Budapest Hotel',
            'annee' => 1938,
        ],[
            'id' => 2,
            'artiste_id' => 2,
            'titre' => 'Isle of dogs',
            'annee' => 1946,
        ]]);
    }
}
