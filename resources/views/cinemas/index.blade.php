@extends('layouts.app')

@section('title', 'My Form, Laravel')

@section('content')
<table>
    <thead>
        <tr>
            <th>{{ __('Cinema') }}</th>
            <th>{{ __('Arrondissement') }}</th>
            <th>{{ __('Adresse') }}</th>
            <th>{{ __('Actions') }}</th>
        </tr>
    </thead>
    <tbody>
        @foreach($cinemas as $cinema)
            <tr>
                <td>{{ $cinema->nom_cinema }}</td>
                <td>{{ $cinema->arrondissement }}</td>
                <td>{{ $cinema->adresse }}</td>
                <td class="table-action">
                    <a type="button" href="{{ route('cinema.edit', $cinema->id) }}" class="btn btn-sm"
                            data-toggle="tooltip" title="@lang('modifier l‘artiste') {{ $cinema->nom }}">
                        <i class="fas fa-edit fa-lg">Edit</i>
                    </a>

                    <a type="button" href="{{ route('cinema.destroy', $cinema->id) }}" class="btn btn-danger btn-sm artiste_destroy"
                        data-toggle="tooltip" title="@lang('suprimer l‘artiste') {{ $cinema->nom }}">
                    <i class="fas fa-edit fa-lg">Destroy</i>
                </a>
                </td>
            </tr>
    @endforeach
    </tbody>
    
</table>
@endsection