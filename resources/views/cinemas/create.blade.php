@extends('layouts.app')

@section('title', 'My Form, Laravel')

@section('content')

<form method="POST" action="{{ route('cinema.store') }}">
    {{ csrf_field() }}

    <p>
        <label for="nom_cinema">Nom du cinema</label>
        <input class="form-control" type="text" name="nom_cinema" id="nom_cinema" value="" require />
        @if ($errors->has('nom_cinema'))
        <div class="invalid-feedback">
            {{ $errors->first('nom_cinema') }}
        </div>
        @endif
    </br>
        <label for="arrondissement">Arrondissement</label>
        <input type="text" name="arrondissement" id="arrondissement" value="" require />
        @if ($errors->has('arrondissement'))
        <div class="invalid-feedback">
            {{ $errors->first('arrondissement') }}
        </div>
        @endif
    </br>
        <label for="adresse">Adresse</label>
        <input type="number" name="adresse" id="adresse" value="" />
        @if ($errors->has('adresse'))
        <div class="invalid-feedback">
            {{ $errors->first('adresse') }}
        </div>
        @endif
    </p>

    <button type="submit">Creer</button>
</form>
@endsection