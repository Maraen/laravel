@extends('layouts.app')

@section('title', 'My Form, Laravel')

@section('content')

<form method="POST" action="{{ route('artiste.update',$artiste->id) }}" enctype="multipart/form-data">
    {{ csrf_field() }}
    {{ method_field('PUT')}} 

    <p>
        <label for="nom">Nom</label>
    <input class="form-control" type="text" name="nom" id="nom" value="{{ $artiste->nom }}" require />

        @if ($errors->has('nom'))
        <div class="invalid-feedback">
            {{ $errors->first('nom') }}
        </div>
        @endif

    </br>
        <label for="prenom">Prenom</label>
        <input type="text" name="prenom" id="prenom" value="{{ $artiste->prenom }}" require />

        @if ($errors->has('prenom'))
        <div class="invalid-feedback">
            {{ $errors->first('prenom') }}
        </div>
        @endif

    </br>
        <label for="annee_naisance">Annee Naissance</label>
        <input type="number" name="annee_naissance" id="annee_naissance" value="{{ $artiste->annee_naissance }}" />

        @if ($errors->has('annee_naissance'))
        <div class="invalid-feedback">
            {{ $errors->first('annee_naissance') }}
        </div>
        @endif
        
    </br>
        <label for="poster">Select image to upload:</label>
        <input type="file" name="poster" id="poster">
    </p>

    <button type="submit">Creer</button>
</form>
@endsection