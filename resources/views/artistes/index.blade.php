@extends('layouts.app')

@section('title', 'My Form, Laravel')

@section('content')
<table>
    <thead>
        <tr>
            <th>{{ __('Nom') }}</th>
            <th>{{ __('Actions') }}</th>
        </tr>
    </thead>
    <tbody>
        @foreach($artistes as $artiste)
            <tr>
                <td>{{ $artiste->nom }}</td>
                <td class="table-action">
                    <a type="button" href="{{ route('artiste.edit', $artiste->id) }}" class="btn btn-sm"
                            data-toggle="tooltip" title="@lang('modifier l‘artiste') {{ $artiste->nom }}">
                        <i class="fas fa-edit fa-lg">Edit</i>
                    </a>

                    <a type="button" href="{{ route('artiste.destroy', $artiste->id) }}" class="btn btn-danger btn-sm artiste_destroy"
                        data-toggle="tooltip" title="@lang('suprimer l‘artiste') {{ $artiste->nom }}">
                    <i class="fas fa-edit fa-lg">Destroy</i>
                </a>
                </td>
            </tr>
    @endforeach
    </tbody>
    
</table>
@endsection