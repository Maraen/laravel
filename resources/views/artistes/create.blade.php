@extends('layouts.app')

@section('title', 'My Form, Laravel')

@section('content')

<form method="POST" action="{{ route('artiste.store') }}">
    {{ csrf_field() }}

    <p>
        <label for="nom">Nom</label>
        <input class="form-control" type="text" name="nom" id="nom" value="" require />
        @if ($errors->has('nom'))
        <div class="invalid-feedback">
            {{ $errors->first('nom') }}
        </div>
        @endif
    </br>
        <label for="prenom">Prenom</label>
        <input type="text" name="prenom" id="prenom" value="" require />
        @if ($errors->has('prenom'))
        <div class="invalid-feedback">
            {{ $errors->first('prenom') }}
        </div>
        @endif
    </br>
        <label for="annee_naisance">Annee Naissance</label>
        <input type="number" name="annee_naissance" id="annee_naissance" value="" />
        @if ($errors->has('annee_naissance'))
        <div class="invalid-feedback">
            {{ $errors->first('annee_naissance') }}
        </div>
        @endif
    </p>

    <button type="submit">Creer</button>
</form>
@endsection