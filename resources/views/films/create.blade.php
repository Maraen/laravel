@extends('layouts.app')

@section('title', 'My Form, Laravel')

@section('content')

<form method="POST" action="{{ route('film.store') }}">
    {{ csrf_field() }}

    <p>
        <label for="titre">Titre</label>
        <input class="form-control" type="text" name="titre" id="titre" value="" require />
        @if ($errors->has('titre'))
        <div class="invalid-feedback">
            {{ $errors->first('titre') }}
        </div>
        @endif
    </br>
        <label for="annee">Annee</label>
        <input type="text" name="annee" id="annee" value="" require />
        @if ($errors->has('annee'))
        <div class="invalid-feedback">
            {{ $errors->first('annee') }}
        </div>
        @endif
    </br>
        <label for="realisateur">Realisateur</label>
        <input type="number" name="artiste_id" id="artiste_id" value="" />
        @if ($errors->has('artiste_id'))
        <div class="invalid-feedback">
            {{ $errors->first('artiste_id') }}
        </div>
        @endif
    </p>

    <button type="submit">Creer</button>
</form>
@endsection