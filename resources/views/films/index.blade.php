@extends('layouts.app')

@section('title', 'My Form, Laravel')

@section('content')
<table>
    <thead>
        <tr>
            <th>{{ __('Titre') }}</th>
            <th>{{ __('Annee') }}</th>
            <th>{{ __('Realisateur') }}</th>
            <th>{{ __('Actions') }}</th>
        </tr>
    </thead>
    <tbody>
        @foreach($films as $film)
            <tr>
                <td>{{ $film->titre }}</td>
                <td>{{ $film->annee }}</td>
                <td>{{ $film->artiste_id }}</td>
                <td class="table-action">
                    <a type="button" href="{{ route('film.edit', $film->id) }}" class="btn btn-sm"
                            data-toggle="tooltip" title="@lang('modifier l‘artiste') {{ $film->nom }}">
                        <i class="fas fa-edit fa-lg">Edit</i>
                    </a>

                    <a type="button" href="{{ route('film.destroy', $film->id) }}" class="btn btn-danger btn-sm artiste_destroy"
                        data-toggle="tooltip" title="@lang('suprimer l‘artiste') {{ $film->nom }}">
                    <i class="fas fa-edit fa-lg">Destroy</i>
                </a>
                </td>
            </tr>
    @endforeach
    </tbody>
    
</table>
@endsection