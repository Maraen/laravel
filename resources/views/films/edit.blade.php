@extends('layouts.app')

@section('title', 'My Form, Laravel')

@section('content')

<form method="POST" action="{{ route('film.update',$film->id) }}" enctype="multipart/form-data">
    {{ csrf_field() }}
    {{ method_field('PUT')}} 

    <p>
        <label for="titre">Titre</label>
    <input class="form-control" type="text" name="titre" id="titre" value="{{ $film->titre }}" require />

        @if ($errors->has('titre'))
        <div class="invalid-feedback">
            {{ $errors->first('titre') }}
        </div>
        @endif

    </br>
        <label for="annee">Annee</label>
        <input type="text" name="annee" id="annee" value="{{ $film->annee }}" require />

        @if ($errors->has('annee'))
        <div class="invalid-feedback">
            {{ $errors->first('annee') }}
        </div>
        @endif

    </br>
        <label for="realisateur">Realisateur</label>
        <input type="number" name="artiste_id" id="artiste_id" value="{{ $film->artiste_id }}" />

        @if ($errors->has('artiste_id'))
        <div class="invalid-feedback">
            {{ $errors->first('artiste_id') }}
        </div>
        @endif
        
    </br>
        <label for="poster">Select image to upload:</label>
        <input type="file" name="poster" id="poster">
    </p>

    <button type="submit">Creer</button>
</form>
@endsection