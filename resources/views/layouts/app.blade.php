<html>
    <head>
        <meta name="csrf-token" content="{{ csrf_token() }}"> 
        <link href="{{ asset('css/app.css') }}?v={{ filemtime(public_path('css/app.css'))}}" rel="stylesheet" type="text/css" />
        <title>@yield('title')</title>
    </head>
    <body>
        @if (session('ok'))
        <div class="container">
            <div class="alert alert-dismissible alert-success fade show" role="alert">
                {{ session('ok') }}
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
        </div>
        @endif



        <div class="container">
            @yield('content')
        </div>
        <footer>
            <script src="{{asset('js/app.js')}}"-?v={{filemtime(public_path('css/app.css'))}}"></script>
        </footer>
    </body>
   
</html>