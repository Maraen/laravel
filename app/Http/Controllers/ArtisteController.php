<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests\ArtisteRequest;
use App\Models\Artiste;
use Illuminate\Support\Facades\Storage;
use Intervention\Image\Facades\InterventionImage as Image;
use App\Policies\ArtistePolicy;


class ArtisteController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //dd(Artiste::where('annee_naissance', '>' ,1980)->get());
        return view('artistes.index', [ 'artistes' => Artiste::all() ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(ArtisteRequest $request)
    {
        return view('artistes.create');


    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(ArtisteRequest $request)
    {
        $artiste = Artiste::create($request->all());
        return redirect()->route('home')
                        ->with('ok', __ ('L‘artiste a bien ete enregistre'));

        $poster = $request->file('poster');
        $filename = 'poster_' . $artiste->id . '.' . $poster->getClientOriginalExtension();
        Image::make($poster)->fit( 180,240 )->save( public_path( '/uploads/posters/' . $filename));

        auth()->user->notify(new ArtisteCreated($artiste));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Artiste $artiste)
    {
        $this->authorize('update',$artiste);

        return view('artistes.edit',['artiste' => $artiste]);

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(ArtisteRequest $request, Artiste $artiste)
    {
        // $this->authorize('artistes.edit',$artiste);

        $artiste->update( $request->all() );

        return redirect()->route('artiste.index')
                        ->with('ok', __('L‘artiste a bien ete modifie'));
        
        $poster = $request->file('poster');
        $filename = 'poster_' . $artiste->id . '.' . $poster->getClientOriginalExtension();
        Image::make($poster)->fit( 180,240 )->save( public_path( '/uploads/posters/' . $filename));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Artiste $artiste)
    {
        $artiste->delete();
        return response()->json();
    }

    public function __construct()
    {
        $this->middleware('ajax')->only('destroy');
        $this->middleware('auth')->only('edit');
    }
}
