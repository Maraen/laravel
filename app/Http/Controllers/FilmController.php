<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests\FilmRequest;
use App\Models\Film;
use Intervention\Image\Facades\InterventionImage as Image;



class FilmController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        
        // dd(Film::whereHas('realisateur')->get());
        // dd( $this->getEloquentSqlWithBindings(Film::whereHas('realisateur')));
        return view('films.index', [ 'films' => Film::all() ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('films.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(FilmRequest $request)
    {
        $film = Film::create($request->all());
        Film::create($request->all());
        return redirect()->route('home')
                        ->with('ok', __ ('L‘film a bien ete enregistre'));

        $poster = $request->file('poster');
        $filename = 'poster_' . $film->id . '.' . $poster->getClientOriginalExtension();
        Image::make($poster)->fit( 180,240 )->save( public_path( '/uploads/posters/' . $filename));

        auth()->user->notify(new FilmCreated($film));
        
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Film $film)
    {
        return view('films.edit',['film' => $film]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(FilmRequest $request, Film $film)
    {
        $film->update( $request->all() );

        return redirect()->route('film.index')
                        ->with('ok', __('L‘film a bien ete modifie'));

        $poster = $request->file('poster');
        $filename = 'poster_' . $film->id . '.' . $poster->getClientOriginalExtension();
        Image::make($poster)->fit( 180,240 )->save( public_path( '/uploads/posters/' . $filename));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Film $film)
    {
        $film->delete();
        return response()->json();
    }

    public function __construct()
    {
        $this->middleware('ajax')->only('destroy');
    }

    public static function getEloquentSqlWithBindings($query)
    {
        return vsprintf(str_replace('?', '%s', $query->toSql()), collect($query->getBindings())->map(function ($binding) {
            return is_numeric($binding) ? $binding : "'{$binding}'";
        })->toArray());
    }
}


