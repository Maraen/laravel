<?php

namespace App\Policies;

use App\Models\User;
use App\Models\Artiste;
use Illuminate\Auth\Access\HandlesAuthorization;

class ArtistePolicy
{
    use HandlesAuthorization;

    // public function edit(User $user, Artiste $artiste){
    //     return $user->id === $artiste->user_id;
    // }
    /**
     * Create a new policy instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    public function before(User $user){
        if($user->is_admin){
            return true;
        }
    }
}
