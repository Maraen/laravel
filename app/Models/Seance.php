<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Seance extends Model
{
    use SoftDeletes;

    protected $fillable = [
        'no_seances','heure_debut', 'heure_fin'
    ];

    public function salle(){
        return $this->belongsTo('App\Models\Room');
    }
}
