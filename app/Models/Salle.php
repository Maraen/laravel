<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Salle extends Model
{
    use SoftDeletes;

    protected $fillable = [
        'no_salle','climatise','capacite'
    ];

    public function cinema(){
        return $this->belongsTo('App\Models\Cinema');
    }

    public function seances(){
        return $this->hasMany('App\Models\Seance');
    }
}
