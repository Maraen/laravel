<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Film extends Model
{
    //use softDeletes
    //TODO make a migration for the soft deletes

    protected $fillable = [
        'titre','annee'
    ];

    public function realisateur(){
        return $this->belongsTo('App\Models\Artiste');
    }

    public function acteurs(){
        return $this->belongsToMany('App\Models\Artiste');
    }
}
