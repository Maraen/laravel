<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Cinema extends Model
{
    //use softDeletes;

    protected $fillable = [
        'nom_cinema','arrondissement','adresse'
    ];

    public function salles(){
        return $this->hasMany('App\Models\Room');
    }
}
